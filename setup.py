#!/usr/bin/env python
from setuptools import setup


def readme():
    with open('README.md', encoding='utf-8') as desc:
        return desc.read()


setup(
    name='pocbible',
    version='1.2',
    description='Python Library to get POC Bible Utilities',
    author='Disen CD',
    long_description=readme(),
    author_email='disencd@gmail.com',
    url='https://gitlab.com/disencd/pocbible',
    packages=['pocbible'],
    license='MIT',
    keywords=[
        'bible',
        'biblepy',
        'pocbible',
    ],
    python_requires='>=3.6',
)
