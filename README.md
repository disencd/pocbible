Daily Bible Documentation
=========================

Python Package that have the below features
    * Gets Verses of todays reading
    * Gets Verses of a specefic date reading - "December 2, 2019"
    * gets the number of bible books, names, chapters, verses

## Git clone repository

git clone https://gitlab.com/disencd/pocbible.git

Python Package that gets the number of bible books, names, chapters, verses

## Installation

```
>>> pip install pocbible
```

## Usage


$ from pocbible import pocbible

$ pocbible.PocBible()
POC Bible

$ bible_obj = pocbible.PocBible()

$ bible_obj.get_number_of_books()
66

$ bible_obj.get_chapter_of_books("John")
21


## Note

Please email with you feedback
    - disencd@gmail.com