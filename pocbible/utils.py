import logging

import bible

from .constants import INDICES
from .exceptions import BibleException

_LOGGER = logging.getLogger(__name__)


def getverses(book_index, chapter_index) -> str:
    """

    :param book_index:
    :param chapter_index:
    :return:
    """
    try:
        chapter_list = [val for val in INDICES if val[0] == book_index]

        _LOGGER.debug(f'chapter_list - {chapter_list}')
        _start_index = [val for val in chapter_list if val[1] == int(chapter_index)][0][
            2
        ]

        _end_index = 0
        for index, val in enumerate(chapter_list):
            if val[1] == int(chapter_index):
                _end_index += chapter_list[index + 1][2]

        chapters = bible.Bible[_start_index:_end_index]
        _LOGGER.info(f'chapters - {chapters}')
        return chapters

    except Exception as e:
        raise BibleException(e)
