import logging
import unittest

import pocbible as poc

logging.basicConfig(
    format='%(asctime)s - %(name)s  %(funcName)s  %(message)s', level=logging.INFO,
)

_LOGGER = logging.getLogger(__name__)


class PocBibleTest(unittest.TestCase):
    def setUp(self):
        self.poc_obj = poc.PocBible()

    def test_get_booknames(self):
        _names = self.poc_obj.get_booknames()
        _LOGGER.info(f'Test get the book names - {_names}')

    def test_get_number_of_books(self):
        _number = self.poc_obj.get_number_of_books()
        _LOGGER.info(f'Test get the number of book - {_number}')

    def test_get_chapter_of_authors(self):
        _number = self.poc_obj.get_chapter_of_books('John')
        _LOGGER.info(
            f'Test get the number of chapters of Book John - {_number}',
        )

        _number = self.poc_obj.get_chapter_of_books('Psalms')
        _LOGGER.info(
            f'Test get the number of chapters of Book Psalms - {_number}',
        )

    # @unittest.expectedFailure
    # def test_error_get_chapter_of_authors(self):
    #     _number = self.poc_obj.get_chapter_of_books('Invalidname')
    #     _LOGGER.info(
    #         f'Test get the number of chapters of Book Joh - {_number}',
    #     )

    def test_read_chapter(self):
        _LOGGER.info(f'Test read_chapter - John 11')
        _str = self.poc_obj.read_chapter('John', 11)
        _LOGGER.info(f'Test read_chapter - John {_str}')

        _LOGGER.info(f'Test read_chapter - Romans 11')
        _str = self.poc_obj.read_chapter('Romans', 5)
        _LOGGER.info(f'Test read_chapter - Romans {_str}')

    def test_read_daily_chapter(self):
        _LOGGER.info(f'test_read_daily_chapter 11/5/2000')
        _str = self.poc_obj.daily_reading('11/5/2025')
        _LOGGER.info(f'Test 2 test_read_daily_chapter {_str}')


if __name__ == '__main__':
    unittest.main()
