import logging

from .calender import Reading
from .constants import ABBR_BOOKLIST
from .constants import BOOKLIST
from .constants import CHAPTERSLIST
from .exceptions import BibleException
from .utils import getverses

_LOGGER = logging.getLogger(__name__)


class PocBible:
    """
    Platform that gets the number of bible books, names, chapters, verses
    """

    def __init__(self):
        pass

    def __repr__(self):
        return 'POC Bible'

    def validate_book_name(input_func):
        """

        :return:
        """

        def decorator_func(*args, **kwargs):
            try:
                if args[1] in BOOKLIST or args[1] in ABBR_BOOKLIST:
                    return input_func(*args, **kwargs)
                else:
                    return 'INVALID BOOK NAME'

            except Exception as e:
                raise BibleException(e)

        return decorator_func

    def parse_chapter_number(input_func):
        """

        :return:
        """

        def decorator_func(*args, **kwargs):
            try:
                if args.__len__() > 2:
                    if ':' in str(args[2]):
                        chapters = args[2].split(':')
                        kwargs['chapter_num1'] = chapters[0]
                        _LOGGER.info(
                            f"Chapter Numbers - {kwargs['chapter_num1']}",
                        )

                return input_func(*args, **kwargs)

            except Exception as e:
                raise BibleException(e)

        return decorator_func

    @staticmethod
    def _validate_book_name(name) -> bool:
        """

        :param name:
        :return:
        """
        return True if name in BOOKLIST or name in ABBR_BOOKLIST else False

    @staticmethod
    def get_number_of_books() -> int:
        """
        Get the number of books in Bible
        :return:
        """
        return BOOKLIST.__len__()

    @staticmethod
    def get_booknames() -> str:
        """
        Get the name of books in Bible
        :return:
        """
        return BOOKLIST.__repr__()

    @validate_book_name
    def get_chapter_of_books(self, name: str) -> int:
        """
        Get the chapter of each books
        :param name:
        :return:
        """
        try:
            # self._validate_book_name(name)
            _LOGGER.info(f'{name} ')
            _index = BOOKLIST.index(name)

            return CHAPTERSLIST[_index][1]
        except Exception as e:
            raise BibleException(e)

    def daily_reading(self, date: str):
        """
        Daily reading based on date - which should be
        "December 16, 2019" format
        :param date:
        :return:
        """
        try:

            if date not in Reading:
                return 'Versus for the Date is Not Supported'

            _chapters = Reading[date]
            _reading_list = _chapters.split(';')
            _LOGGER.info(f'Todays chapter are {_reading_list}')
            for _reading in _reading_list:
                _val = _reading.split()
                self.read_chapter(_val[0], _val[1])

            return _reading_list
        except Exception as e:
            raise BibleException(e)

    # Beta version
    @validate_book_name
    @parse_chapter_number
    def read_chapter(self, name: str, chapter_num: str, chapter_num1=0) -> str:
        """
        Read the complete chapter
        :param name:
        :param chapter_num:
        :return:
        """
        try:

            # self._validate_book_name(name)
            _LOGGER.debug(f'{name} {chapter_num} {chapter_num1}')
            _index = BOOKLIST.index(name) + 1

            _LOGGER.debug(f'{name} index is {_index}')

            if chapter_num1:
                return getverses(_index, chapter_num1)

            return getverses(_index, chapter_num)

        except Exception as e:
            raise BibleException(e)
