class BibleException(Exception):
    __slots__ = ['input']

    def __init__(self, input):
        self.input = input

    def __str__(self):
        return '{} match not found in Bible'.format(self.input.capitalize())
